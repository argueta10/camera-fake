# Camera Fake en Ubuntu 18.04 y gst-launch-1.0

Este tutorial explica el proceso para instalar una camara fake desde una virtualización en linux. -Actualizado 4 de junio 2019

# Install v4l2loopback

```
  $ wget https://github.com/umlaeute/v4l2loopback/archive/master.zip
  $ unzip master.zip
  $ cd v4l2loopback-master
  $ make
  $ sudo make install
  $ sudo modprobe v4l2loopback video_nr=0 card_label="mockCam"
```

# Dependencias que se pueden llegar a ocupar al instalar v4l2loopback
```
    $ sudo depmod -a
    $ sudo apt install linux-generic
    $ sudo apt install v4l2loopback-dkms
    $ sudo apt install linux-modules-extra-gcp
    $ sudo apt install v4l2loopback
    $ sudo apt install v4l-utils
    $ chmod -R 777 /dev/video*
    $ v4l2-ctl --list-devices
```

# Emulando una imagen

```
  $ wget "https://chart.googleapis.com/chart?chs=600x340&cht=qr&chl=testing" -O qr.png
  $ gst-launch-1.0 filesrc location=qr.png ! pngdec ! imagefreeze ! v4l2sink device=/dev/video0
```

# Emulando el escritorio

```
  $ gst-launch-1.0 ximagesrc startx=1280 use-damage=0 ! video/x-raw,framerate=30/1 ! videoscale method=0 ! video/x-raw,width=640,height=480  ! v4l2sink device=/dev/video0
```

# Instalar Android Studio

```
  - Descargar android studio
  - extraer .zip
  $ sudo mv /home/forge/Downloads/android-studio /usr/local
  $ cd /usr/local/android-studio/bin
  $ ./studio.sh
```
- Add icon to Desktop

# Install qemu-kvm

```
  $ sudo apt install qemu-kvm
  $ sudo adduser $USER kvm
  $ sudo chown $USER /dev/kvm
```

  - abrir Android Studio del menu inicio
  - configure -> ADV manager
  - Create Nexus_5_API_25


# Customizando emulador
- Optimizacion de la virtualizacion https://github.com/intel/haxm

```
  $ sudo apt install nasm
  $ wget https://github.com/intel/haxm/archive/v7.5.1.zip
  $ unzip v7.5.1.zip
  $ cd haxm-7.5.1/platforms/linux/
  $ make
  $ sudo modprobe kvm-intel
  $ sudo reebot
```


# Install whatsAppbussines

```
  - Ejecutar emulador
  $ cd Android/Sdk/emulator
  $ ./emulator -avd Nexus_5_API_25 -memory 4096 -cores 3 -gpu swiftshader_indirect
  - Crear cuenta en play store
  - Descargar WhatsApp Business
  - Dar de alta whatsApp y escanear codigo en web.whatsapp.com
  - Actualizar todas las app desde play store
```











